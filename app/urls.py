from django.urls import path

from app import views

urlpatterns = [
    path('', views.index, name='index'),
    path('upload/', views.UploadView.as_view(), name='upload'),
    path('<str:hash>/', views.show, name='show'),
]


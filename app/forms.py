from django import forms
from django.core.exceptions import ValidationError


class UploadForm(forms.Form):
    file = forms.ImageField(label='Файл', required=False)
    url = forms.CharField(label='Ссылка', required=False)

    def clean(self):
        super().clean()
        data = self.cleaned_data
        file = data.get('file')
        url = data.get('url')
        if bool(file) ^ bool(url) is False:
            raise ValidationError('Требуется заполнить ровно одно из полей')

import requests
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import FormView

from app import models
from app.forms import UploadForm


def index(request):
    images = models.Image.objects.all()
    return render(request, 'app/index.html', {'images': images})


def show(request, hash):
    image = get_object_or_404(models.Image, hash=hash)
    width = request.GET.get('width')
    height = request.GET.get('height')
    size = request.GET.get('size')

    width = int(width) if width else image.file.width
    height = int(height) if height else image.file.height
    size = int(size) if size else image.file.size

    image = image.get_resized(width, height, size)

    return render(request, 'app/show.html', {'image': image})


class UploadView(FormView):
    template_name = 'app/upload.html'
    form_class = UploadForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        file = form.cleaned_data.get('file')
        if file:
            hash = models.hash_file(file)
            image = models.Image(file=file, hash=hash)
            image.save()
        else:
            url = form.cleaned_data.get('url')
            data = requests.get(url).content

            temp = NamedTemporaryFile(delete=True)
            temp.write(data)
            temp.flush()

            hash = models.hash_file(temp)
            image = models.Image(hash=hash)
            image.file.save(hash, File(temp))
            image.save()

        return super().form_valid(form)

# Generated by Django 3.0.7 on 2020-06-25 14:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResizedImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.ImageField(height_field='height', upload_to='images', width_field='width')),
                ('width', models.PositiveIntegerField()),
                ('height', models.PositiveIntegerField()),
                ('max_size', models.PositiveIntegerField()),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sizes', to='app.Image')),
            ],
        ),
    ]

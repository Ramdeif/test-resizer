import hashlib
import sys
from functools import partial
from io import BytesIO

import PIL.Image
from django.core.files import File
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models


def hash_file(file, block_size=65536):
    hasher = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b''):
        hasher.update(buf)

    return hasher.hexdigest()


class Image(models.Model):
    file = models.ImageField(upload_to='images')
    hash = models.CharField(max_length=256)

    def get_resized(self, width, height, max_size):
        # return self if it fits requirements
        if self.file.width == width and self.file.height == height and \
                self.file.size <= max_size:
            return self

        # try to find in cache
        images = self.sizes.filter(width=width, height=height, max_size=max_size)
        if images.exists():
            return images.first()

        # generate resized image
        self.file.open()
        im = PIL.Image.open(self.file)
        im2 = im.resize((width, height))

        output = BytesIO()
        quality = 100
        im2.save(output, format=im.format,  quality=quality)

        # Compressing JPG
        if im.format == 'JPEG':
            while sys.getsizeof(output) > max_size and quality > 5:
                quality -= 5
                output = BytesIO()
                im2.save(output, format=im.format, quality=quality)

        output.seek(0)

        content_type = 'image/png' if im.format == 'PNG' else 'image.jpeg'
        file = InMemoryUploadedFile(output, 'ImageField',
                                    self.file.name, content_type,
                                    sys.getsizeof(output), None)

        resized = ResizedImage(source=self,
                               file=File(file),
                               width=width,
                               height=height,
                               max_size=max_size)
        resized.save()
        self.file.close()
        return resized


class ResizedImage(models.Model):
    source = models.ForeignKey(Image,
                               related_name='sizes',
                               on_delete=models.CASCADE)
    file = models.ImageField(upload_to='images',
                             width_field='width',
                             height_field='height')
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()
    max_size = models.PositiveIntegerField()
